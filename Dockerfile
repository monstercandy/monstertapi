FROM monstercommon

ADD lib /opt/MonsterTapi/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterTapi/lib/bin/tapi-install.sh && /opt/MonsterTapi/lib/bin/tapi-test.sh

ENTRYPOINT ["/opt/MonsterTapi/lib/bin/tapi-start.sh"]
