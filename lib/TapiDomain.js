const path = require("path");
const dotq = require("MonsterDotq");
const fs = dotq.fs();
const lockfile = require("MonsterLockfile");
const MError = require('MonsterError');;
const TapiRecords = require('TapiRecords.js');


function TapiDomain(domainName, config, directory, extraParams) {
  var m = { "records": [] }

  if(!directory) directory = config.tinyDnsDataDir()

  if(!domainName) throw new MError("DOMAIN_NAME_MISSING")

  if(!require('MonsterValidators').IsValidDomain(domainName))
    throw new MError("INVALID_DOMAIN")

  m["domain"] = domainName

  var domainFn = path.join(directory, domainName) + "." + require("constants.js").DOMAIN_EXT
  var tmpDomainFn =  path.join(directory, '.'+domainName)

  m.Delete = function(readyCallback) {
    fs.unlink(domainFn, readyCallback)
  }

  m.Create = function(readyCallback) {
      fs.stat(domainFn, function(err, stats){
         if(!err)
            readyCallback(new Error("DOMAIN_ALREADY_EXISTS"))

         if(err) {
            // it does not exists, and this is expected
            fs.open(domainFn, "w", 0600, function(err, fd){
                if(err)
                    return readyCallback(err)

                fs.close(fd, function(){
                    return readyCallback()
                });
            });
         }

      });
  }

  m.Exists = function(readyCallback)  {
      fs.stat(domainFn, function(err, stats){
          if(err) return readyCallback(false)

          readyCallback(true)
      })

  }


  m.Parse = function(readyCallback) {

        m.records = []


        require("FirstLineAsJSON")(domainFn, function(err, result){
           if(err) return readyCallback(err)

           readyCallback(null, m.records)

        }, function(line, originalParser){
           var recs = originalParser(line)

           if(!Array.isArray(recs))
              throw new MError("INVALID_JSON_IN_DOMAIN_FILE")

           for (q of recs) {
              m.records.push( TapiRecords.FromJSON(q, config, directory, m.domain, extraParams) )
           }

        })


  }

  m.AsTinyDnsData = function(){
        var re = ""
        for(var q of m.records) {
            re += q.AsTinyDnsData(m["domain"])+"\n"
        }
        return re
  }

  function doChmod(mode, readyCallback){
     return fs.chmod(domainFn, mode, readyCallback)
  }
  m.Lock = function(readyCallback){
    return doChmod(0400, readyCallback)
  }
  m.Unlock = function(readyCallback){
    return doChmod(0600, readyCallback)
  }

  m.Flush = function(timestamp, readyCallback){

          var maxRecords = config.get("max_number_of_records_per_zone")
          if((m.records)&&(m.records.length > maxRecords)) {
             return readyCallback(new MError("TOO_MANY_RECORDS", Array(m.records.length + " > " + maxRecords)))
          }

          return fs.statAsync(domainFn)
            .catch((err)=>{
               // this is swalloed purposefully
               if(err.code == "ENOENT") return;

               throw err;
            })
            .then(stats=>{
                if((stats)&&((stats.mode == 33024)||(stats.mode == 33060))) { // the two number here are 0400 in Linux and read-only file in Windows
                  throw new MError("DOMAIN_IS_LOCKED");
                }

                var content = "#"  + JSON.stringify(m.AsJSON()) + "\n";
                for(var q of m.records) {
                    content += q.AsTinyDnsData(m["domain"])+"\n";
                }

                return lockfile.logic({lockfile: domainName, callback: function(lockfilePath){

                    return fs.writeFileAsync(domainFn, content, {mode:0600})
                    .then(()=>{
                        return fs.utimesAsync(domainFn, timestamp, timestamp)
                    })

                }})
            })
            .then(()=>{
                return readyCallback();
            })
            .catch(readyCallback);


  }


  m.AsJSON = function(withHash){
     var re = [];

     for(q of m.records) {
        var r = q.AsJSON()
        if(withHash)
          r["hash"] = TapiRecords.Hash(q)
        re.push(r)
     }

     return re
  }

  return m
}

module.exports = TapiDomain

