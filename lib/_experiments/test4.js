const util = require('util')
var express = require('express');
var app = express();
var fs = require("fs");


app.get('/listUsers', 
   function (req, res, next) {
      fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
   
         next( new Error('failed to load user') );
	  
     });
   }
)

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!: '+err);
});


var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  util.log("Example app listening at http://%s:%s", host, port)

})
