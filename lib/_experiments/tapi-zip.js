
module.exports = function(app, config) {
   
   const fs = require("fs")
   const path = require("path")
   const regen = require("regen-logic.js")
   const MError = require('MonsterError')
   const RecordCommon = require('MonsterValidators')

	app.post("/import/zip", function (req, res, next) {

	  var myRegexp = /(.+)\/(.+)/;
	  var j = req.body.json

	  try{

	     var dirs = getTinyDnsDataDirs()
		 var yauzl = require("yauzl");

		 var buf = new Buffer(j["zip_data"], 'base64');
		 yauzl.fromBuffer(buf, {lazyEntries: true}, function(err, zipfile) {
		 	if(err) return next(new MError("ZIP_INVALID_ENTRY", null, err))

		     var readDirAll = dirs.length
		     var readDirReady = 0

		     regen.regenerateTinydnsDataLogic(function(){


		          var allDataFiles = 0
		          var deletedDataFiles = 0

		          Object.keys(dirs).forEach((category)=>{
		            var dir = dirs[category]

		            fs.readdir(dir, function(err, files){
		               if(err) return mynext(new MError("DATA_DIR_READ_ERROR", null, err))

		               readDirReady++

		               var dataFiles = files.filter((x)=>{ return RecordCommon.LooksValidDataFile(x) })
		               allDataFiles += dataFiles.length

		               if((0 >= allDataFiles)&&(readDirReady>=readDirReady))
		                   doExtraction(zipfile)

		               for(dataFile of dataFiles) {
		                  var fullDataFilePath = path.join(dir, dataFile)
		                  // console.log("unlinking "+fullDataFilePath)

		                  fs.unlink(fullDataFilePath, (err)=>{
		                     if(err) return mynext(new MError("ERROR_DELETIG_OLD_SET", null, err))

		                     deletedDataFiles++

		                     if((deletedDataFiles >= allDataFiles)&&(readDirReady>=readDirReady))
		                         doExtraction(zipfile)


		                  })//fs.unlink ready

		                }//dataFiles iteration


		            })//readDir callback



		          })// dirs iteration



		     })



		 })

		 function mynext(p1, p2, p3) {

		  	  next(p1, p2, p3)
              regen.regenerateTinydnsDataStub(config)            

		 }


	     function doExtraction(zipfile) {

              var extracted= 0

			  zipfile.once("end", function() {
	               req.result = {"extracted": extracted}
	               return mynext()               

  			  });
			  zipfile.on("entry", function(entry) {

			  	  var x = entry.fileName

                  // console.log("Extracting: ", x);

                  var match = myRegexp.exec(x);
                  if(!match) {
                     console.log("Non matching regexp?");

                     return mynext (new MError("ZIP_INVALID_ENTRY"))
                  }

                  var dir = dirs[match[1]]
                  if(!dir) {
                     console.log("Invalid directory?");

                     return mynext (new MError("ZIP_INVALID_ENTRY"))
                  }
                  var domain = match[2]

                  if(!RecordCommon.IsValidDomainFile(domain)) {
                     console.log("Invalid domain file?");

                     return mynext (new MError("ZIP_INVALID_ENTRY"))
                  }

                  var fullPath = path.join(dir, domain)
                  // console.log("Extracting to: ", fullPath);

			      
			      zipfile.openReadStream(entry, function(err, readStream) {
				          if (err) return mynext(new MError(ZIP_INVALID_ENTRY, null, err));

				          var ws = fs.createWriteStream(fullPath)

				          ws.on("finish", function(){

					          var ts = entry.getLastModDate().unixtime()

					          fs.utimes(fullPath, ts, ts, function(){
					          });

				          })

				          readStream.on("end", function() {

		                      extracted++

					          zipfile.readEntry();
				          });

				          readStream.pipe(ws);

			        });
			        
			  });
			  zipfile.readEntry();



	     }


	   }catch(ex) {
	      next(new MError("ZIP_EXTRACT_ERROR", null, ex))
	   }



	})


	app.get("/export/zip", function (req, res, next) {
	   
	  var dirs = getTinyDnsDataDirs()
	  var readDirAll = dirs.length
	  var readDirReady = 0

	  var allFiles = 0
	  var addedFiles = 0
	  var yazl = require("yazl");
	  var zipfile = new yazl.ZipFile();

	  var chunks = [];
	  zipfile.outputStream.on("data", function(chunk) {
	    chunks.push(chunk);
	  }).on("end", function(){
	    var result = Buffer.concat(chunks);
	    req.result = {"zip_data": result.toString('base64')}
	    next()
	  });


	  Object.keys(dirs).forEach((category)=>{
	    var dir = dirs[category]
	    fs.readdir(dir, function(err, files){
	        if(err) return next(new MError("DATA_DIR_READ_ERROR", null, err))

	        var re = config.filterArrayForZoneFiles(files, true)
	        allFiles += re.length
	        readDirReady++

	        re.forEach((x)=>{
	           var fullPath = path.join(dir, x)

	           addedFiles++
	           // console.log("adding "+category+" "+x+" in export ("+addedFiles+"/"+allFiles+")")

	           zipfile.addFile(fullPath, path.join(category, x));

	           if((addedFiles >= allFiles) && (readDirReady >= readDirAll)) {
	              zipfile.end();
	           }

	        })


	    })
	  })



	});


	function getTinyDnsDataDirs(){
	  var dirs = new Array()
	  dirs["preload"] = config.tinyDnsPreloadDataDir()
	  dirs["data"] = config.tinyDnsDataDir()
	  return dirs
	}	

}
