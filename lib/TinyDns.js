
var exports = module.exports = {}

exports.ConvertByteToOctetString = function(ccode) {
      if(
         (ccode != 0x3a) /* : */
         &&
         (((ccode > 0x2F)&&(ccode < 0x80)) || (ccode == 0x2B /* + */))
        )
         return String.fromCharCode(ccode)
       else
         return exports.ConvertNumberToOctetString(ccode)
}
exports.ConvertCharacterToOctetString = function(character) {
      return exports.ConvertByteToOctetString( character.charCodeAt(0) )
}
exports.ConvertNumberToOctetString = function(n) {
   return "\\" +	exports.pad(n.toString(8), 3)
}

exports.ConvertByteArrayToOctetStrings = function(b) {

  var olen = b.length
    var text_encoded = ""
    for(var i = 0; i < olen; i++) {
      text_encoded += exports.ConvertByteToOctetString(b[i])
    }

    return text_encoded
}
exports.ConvertBinaryToOctetStrings = function(b) {

	var olen = b.length
    var text_encoded = ""
    for(var i = 0; i < olen; i++) {
      var c = b.charAt(i)
      text_encoded += exports.ConvertCharacterToOctetString(c)
    }

    return text_encoded
}
//	:default._domainkey.index.hu:16:\352v=DKIM1;\040k=rsa;\040p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIHJ0sLqhHb1he3WhfCtP8+6ItJm\0573EDUtZk+sUgC77FT9jjwEFwbvy2b3dW1X9sKS4BkEfft9ScN01pNbhZkUSYPBaS45a+p8ZXWbJBYfw4EiHrvsa5tzAfmbEXbBPQHyQ4wsml8MBpQ\057ofTwD8JWHc+slW03DSrMLKM+\0578369QIDAQAB:3600

exports.pad = function(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

exports.ConvertLongStringToTinydns = function(str) {
   var b = ""
   var s = str
       while(s.length > 0) {
          var as = s.substring(0,255)
          var slen = as.length

          // console.log("foo", as, slen)

          var text_encoded = exports.ConvertBinaryToOctetStrings(as)

          b += exports.ConvertNumberToOctetString(slen)
          b += text_encoded

          s = s.substring(255)
       }

       return b
}