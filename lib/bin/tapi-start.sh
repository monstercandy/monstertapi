#!/bin/bash

. /opt/MonsterCommon/lib/common.inc

mkdir /var/lib/tinydns/root/data.d || true
mkdir /var/lib/tinydns/root/preload.d || true
mkdir /var/lib/tinydns/root/template.d || true
chown -R 10000:10000 /var/lib/tinydns/root "/var/log/monster/$INSTANCE_NAME" "/var/lib/monster/$INSTANCE_NAME"

mc_start
