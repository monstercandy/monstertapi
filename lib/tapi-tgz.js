
var fs = require("fs")

module.exports = function(router, app) {

  var config = app.config

  var directoryKeys = ["preload", "data", "template"]

   const path = require("path")
   const dotq = require("MonsterDotq");
   const fs = dotq.fs();
   const regen = require("regen-logic.js")
   const MError = require('MonsterError')
   const Validators = require('MonsterValidators')

   const firstLine = require("FirstLineAsJSON");
   const constants = require("constants.js")

	router.post( "/import/tgz", function (req, res, next) {
		"use strict"

	  var j = req.body.json

      var dirs = config.getTinyDnsDataDirs()

      var dirsToExtract = Array()
      for(let q of directoryKeys) {
     	   if(j[q])
     		   dirsToExtract.push(q)
  	  }


      app.InsertEvent(req, {e_event_type: "data-import"})


  	  var filesInvalid = 0
      var filesExtracted = 0

      var tinyDnsRegen = false
      var streams = require("MonsterStreams")
  	  regen.regenerateTinydnsDataLogicAsync()
      .then(()=>{
          if(j.partial) return;
          return re.CleanupDirs(dirsToExtract)
      })
      .then((pathes)=>{
         var promises = []
         for (var i = 0; i < dirsToExtract.length; ++i) {
              let q = dirsToExtract[i]
              var p = new Promise(function(resolve,reject){

                      let buf = streams.Base64ToBuffer(String(j[q]))

                      let tgzInput = streams.ExtractTgz(dirs[q], {
                           ignore: function(name, header) {
                              if(header.type !== 'file') {
                                    filesInvalid++
                                    return true
                              }

                              var pureName = path.basename(name)
                              if(!Validators.IsValidDomainFile(pureName)) {
                                     console.log("invalid domain:", name)
                                     filesInvalid++
                                 return true
                              }
                              if(path.join(dirs[q], pureName) != name) {
                                     console.log("invalid path:", name)
                                     filesInvalid++
                                 return true
                              }

                              filesExtracted++

                              return false
                           }

                      })
                      let tarStream = tgzInput.tarStream

                      var bs = new streams.BufferStream(buf)
                      bs.pipe(tgzInput)

                      tgzInput.on("error", function(err){
                            reject(err)
                      })


                      tarStream.on("finish", function(){
                            resolve()
                      })


              })
              promises.push(p);
         }

         return Promise.all(promises)

      })
      .then(()=>{

         // "invalid": filesInvalid,
         req.result = {
          "extracted": filesExtracted
         }

         if(dirsToExtract.length) {
            regen.regenerateTinydnsDataStub(config)
            tinyDnsRegen = true
         }


         next()

      })
      .catch((ex)=>{
         next(new MError("ERROR_WHILE_EXTRACTING", null, ex))

      })
      .then(()=>{
         if(!tinyDnsRegen)
           regen.tinyDnsFinisher()
      })


	})


  router.get( "/export/json", function (req, res, next) {

      var dirs = config.getTinyDnsDataDirs()

      var dirsPacked = 0
      var dirsToPack = Array()
      var streams = require("MonsterStreams")

      var re = {};
      app.InsertEvent(req, {e_event_type: "data-export"})
      return fs.readdirAsync(dirs.data)
        .then(files=>{

            var regexp = /(.+)\.z$/;
            var zones = files.filter(x => ((x.endsWith(constants.DOMAIN_EXT))&&(x != "__parent_child.z")));

            return dotq.linearMap({array: zones, catch: true, action: function(zoneFn){

               var m = regexp.exec(zoneFn);
               if(!m) return;
               var domain = m[1];

               var fullFn = path.join(dirs.data, zoneFn);

               re[domain] = {};
               return fs.statAsync(fullFn)
                 .then(stat=>{
                    re[domain].mtime = stat.mtimeMs;
                    return firstLine.ReadAsync(fullFn)
                 })
                 .then(function(j){
                    re[domain].records = j;
                 });

            }})

        })
        .then(()=>{
           return req.sendResponse(re);
        })

  });


  Array("get","post").forEach(function(method){
      router[method]( "/export/tgz", function (req, res, next) {
        "use strict"

        var dirs = config.getTinyDnsDataDirs()

          req.result = {}
          var dirsPacked = 0
          var dirsToPack = Array()
          var streams = require("MonsterStreams")

          app.InsertEvent(req, {e_event_type: "data-export"})

          var tgzOptions = {};

          if(Array.isArray(req.body.json.zones)) {

             req.result.partial = true;

             req.body.json.zones.forEach(x=>{
                if(!x.cat) x.cat = "data";
                if(directoryKeys.indexOf(x.cat) < 0)
                   throw new MError("INVALID_CATEGORY");

                var zoneName = x.zone;
                if((!zoneName)||(!Validators.IsValidDomain(zoneName))) 
                  throw new MError("INVALID_ZONE");

                if(dirsToPack.indexOf(x.cat) < 0)
                   dirsToPack.push(x.cat);

                if(!tgzOptions[x.cat])
                   tgzOptions[x.cat] = { entries:[] };

                tgzOptions[x.cat].entries.push(zoneName+".z");
             });


          } else {

            for(let q of directoryKeys) {
                try {
                if(fs.statSync(dirs[q]))
                   dirsToPack.push(q)
              } catch(ex){
              }
            }

          }


          for(let q of dirsToPack)
          {
            streams.StreamToBase64( streams.PackTgz(dirs[q], tgzOptions[q]), function(b64) {
              b64helper(q, b64)
            })
          }


          function b64helper(cat, b64) {
             dirsPacked++
             req.result[cat] = b64
             if(dirsPacked >= dirsToPack.length)
             {
                req.sendResponse(req.result);
             }
          }

      });
  });



  var re = {}
  re.CleanupDirs = function(dirsToCleanup) {
        var dirs = config.getTinyDnsDataDirs()
        var toDel = dirsToCleanup.map((x)=>{
           if(!dirs[x]) throw new Error("Invalid parameters")

           return path.join(dirs[x], "*."+constants.DOMAIN_EXT)
        })

        const del = require('MonsterDel');
        return del(toDel,{"force": true})
  }

  return re

}
