const TapiRecordA = require("records/TapiRecordA.js")
const TapiRecordAPTR = require("records/TapiRecordAPTR.js")
const TapiRecordPTR = require("records/TapiRecordPTR.js")
const TapiRecordCNAME = require("records/TapiRecordCNAME.js")
const TapiRecordMX = require("records/TapiRecordMX.js")
const TapiRecordTXT = require("records/TapiRecordTXT.js")
const TapiRecordSOANSA = require("records/TapiRecordSOANSA.js")
const TapiRecordNSA = require("records/TapiRecordNSA.js")
const TapiRecordSOAContact = require("records/TapiRecordSOAContact.js")
const TapiRecordGeneric = require("records/TapiRecordGeneric.js")
const TapiRecordAAAA = require("records/TapiRecordAAAA.js")
const TapiRecordSRV = require("records/TapiRecordSRV.js")
const TapiRecordCAA = require("records/TapiRecordCAA.js")
const TapiRecordClone = require("records/TapiRecordClone.js")

var exports = module.exports = {}

exports.FromJSON = function(json, config, directory, mainDomain, extraParams) {
  if(!extraParams) extraParams = {};
	if(!json["type"])
		throw new Error("RECORD_TYPE_NOT_SPECIFIED")
	switch(json["type"]) {
       case "A":
          return TapiRecordA(json, config, mainDomain, extraParams);
       case "A/PTR":
          return TapiRecordAPTR(json, config, mainDomain, extraParams);
       case "PTR":
          return TapiRecordPTR(json, config, mainDomain, extraParams);
       case "CNAME":
          return TapiRecordCNAME(json, config, mainDomain, extraParams);
       case "MX":
          return TapiRecordMX(json, config, mainDomain, extraParams);
       case "TXT":
          return TapiRecordTXT(json, config, mainDomain, extraParams);
       case "SOA/NS/A":
          return TapiRecordSOANSA(json, config, mainDomain, extraParams);
       case "NS/A":
          return TapiRecordNSA(json, config, mainDomain, extraParams);
       case "SOA":
          return TapiRecordSOA(json, config, mainDomain, extraParams);
       case "Generic":
          return TapiRecordGeneric(json, config, mainDomain, extraParams);
       case "AAAA":
          return TapiRecordAAAA(json, config, mainDomain, extraParams);
       case "SRV":
          return TapiRecordSRV(json, config, mainDomain, extraParams);
       case "CAA":
          return TapiRecordCAA(json, config, mainDomain, extraParams);
       case "CLONE":
          return TapiRecordClone(json, config, mainDomain, extraParams);
       case "SOA-Contact":
          if(directory == config.tinyDnsPreloadDataDir())
             return TapiRecordSOAContact(json, config, mainDomain, extraParams);
       default:
          throw new Error("RECORD_TYPE_UNKNOWN")
	}
}

exports.FromJSONArray = function(json, config, directory, mainDomain, extraParams) {
  var re = Array()
  if(require("util").isArray(json)) {

      for(q of json) {
         if(json.dontResolve)
            q.dontResolve = true;
         var r = exports.FromJSON(q, config, directory, mainDomain, extraParams)
         re.push(r)
      }

  }else
    re.push(exports.FromJSON(json, config, directory, mainDomain, extraParams))

  return re
}

exports.Hash = function(record) {

  const crypto = require('crypto');
  const hash = crypto.createHash('sha256');

  hash.update(JSON.stringify(record.AsJSON()));
  return hash.digest("hex")

}
