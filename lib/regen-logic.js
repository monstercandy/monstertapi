var dataModifierStubIsRunning = 0
var tinyDnsDataWantsRunning = 0
var regenerateTimer
var modifierQueue = Array()
var tinyDnsQueue = Array()

const MError = require("MonsterError")

var exports = module.exports = {};

exports.waitModifierLogic= function(next, config, workerCallback){

  var queueItem = {}
  var stub = function(){
    if(queueItem["timer"]){
       clearTimeout(queueItem["timer"])
       queueItem["timer"] = null
    }
    dataModifierStubIsRunning++    
    workerCallback()
    dataModifierStubIsRunning--

    if(!regenerateTimer)
      regenerateTimer = setTimeout(()=>{ regenerateTinydnsData(config); }, config.get("tinydns_data_regenerate_delay_msec"));

    if((dataModifierStubIsRunning <= 0)&&(tinyDnsQueue.length > 0)){

      for (q of tinyDnsQueue) {
         q()         
      }
      tinyDnsQueue = Array()
    }
  }

  if(tinyDnsDataWantsRunning > 0) {

      console.log("Need to wait for tinydns-data")

      queueItem["timer"] = setTimeout(function(){
           queueItem["stub"] = null
           next(new MError("TIMEOUT_WAITING_FOR_TINYDNS_DATA"))
      }, config.get("max_wait_for_tinydns_data_before_request_timeout_msec"))
      queueItem["stub"] = stub

      modifierQueue.push(queueItem)

      return;
  }


  stub()

}
function regenerateTinydnsData(config){
   exports.regenerateTinydnsDataLogic(()=>{
    exports.regenerateTinydnsDataStub(config)
  })
}
exports.regenerateTinydnsDataLogicAsync = function(){
   return new Promise(function(resolve, reject){
       exports.regenerateTinydnsDataLogic(function(){
          resolve()
       })
   })
}
exports.regenerateTinydnsDataLogic = function(tinyDnsCallback){
   tinyDnsDataWantsRunning++

   if(dataModifierStubIsRunning > 0)
   {
      console.log("Need to wait for requests to be finished before running tinydns-data")

      tinyDnsQueue.push(tinyDnsCallback)
      return
   }

   tinyDnsCallback()

}
exports.regenerateTinydnsDataStub=function(config, callback){

      clearTimeout(regenerateTimer);
      regenerateTimer = null

      var constants = require("constants.js")
      var exec = require('child_process').exec;
      var cmd = config.get("cmd_tinydns_data")

      console.log("Invoking tinydns-data: ",cmd)
      var child = exec(cmd,
        function (error, stdout, stderr) {
            if(error) console.error("Error while regenerating tinydns data file", error, stdout, stderr)

            exports.tinyDnsFinisher()

        }).on("exit", (code, signal)=>{
            if(code)
              console.error("tinydns-data regeneration returned failure! Exit code: ", code)

            if(callback)
               callback(code);
        });

}
exports.tinyDnsFinisher = function() {
      console.log("Resuming waiting modifier requests: "+modifierQueue.length)
      for (q of modifierQueue) {
         if(q["stub"])
            q["stub"]()
         
      }
      modifierQueue = Array()
      tinyDnsDataWantsRunning--
}
