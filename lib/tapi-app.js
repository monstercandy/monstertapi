// permissions needed: ["EMIT_LOG"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "tapi"

    const regen = require("regen-logic.js")

    const path = require('path')
    const util = require('util');
    const dotq = require("MonsterDotq");
    var fs = dotq.fs();


    var config = require('MonsterConfig');
    var me = require('MonsterExpress');
    const MError = me.Error

    const TapiDomain = require('TapiDomain.js');
    const TapiRecords = require("TapiRecords.js")

    const DEFAULT_DOMAIN = "default.xxx"
    var constants = require("constants.js")


    config.appRestPrefix = "/tapi"

    config.defaults({
      "tinydns_root_directory":  (process.env.NODE_ENV == "development" ?  __dirname : "/etc/tinydns/root"),
      "tinydns_data_directory": "data.d",
      "tinydns_preload_directory": "preload.d",
      "tinydns_template_directory": "template.d",
      "tinydns_data_regenerate_delay_msec": 60000,
      "cmd_tinydns_data": "/usr/local/sbin/dtinydns-data",
      "clone_record_support": false,
      "strict_mx_records": true,
      "dns_record_default_ttl": 3600,
      "max_wait_for_tinydns_data_before_request_timeout_msec": 10000,
      "max_txt_record_length": 1024,
      "max_data_record_length": 1024,
      "max_number_of_records_per_zone": 100,
    })

    function getTinyDnsFullDir(subdir) {

      return path.join( config.get("tinydns_root_directory"), subdir )

    }
    config.tinyDnsDataDir = function() {
      return getTinyDnsFullDir(config.get("tinydns_data_directory"))
    }
    config.tinyDnsPreloadDataDir = function() {
      return getTinyDnsFullDir(config.get("tinydns_preload_directory"))
    }
    config.tinyDnsTemplateDir = function() {
      return getTinyDnsFullDir(config.get("tinydns_template_directory"))
    }
    config.getTinyDnsDataDirs = function(){
      var dirs = new Array()
      dirs["preload"] = config.tinyDnsPreloadDataDir()
      dirs["data"] = config.tinyDnsDataDir()
      dirs["template"] = config.tinyDnsTemplateDir()
      return dirs
    }
    config.getParentChildMapFilePath = function(){
      return path.join(config.tinyDnsDataDir(), "__parent_child.z");
    }

    config.filterArrayForZoneFiles = function(files, skipMapping){
       var re = files.filter((x)=>{ 
          if(x.match(/^_/)) return false;
          return x.substr(-1*(constants.DOMAIN_EXT.length+1)) == "." + constants.DOMAIN_EXT;
       })
       if(!skipMapping)
         re = re.map((x)=>{ return x.substr(0, x.length-constants.DOMAIN_EXT.length-1) })
       return re
    }



    var app = me.Express(config, moduleOptions);


    var router = app.PromiseRouter(config.appRestPrefix)

    router.route( "/soa-contact")
      .get(function (req, res, next) {
           readDomain(DEFAULT_DOMAIN, next, (dom)=>{
              var contact = ""
              if(dom["records"].length > 0)
                 contact = dom["records"][0]["contact"]
              req.result = {"contact": contact }
              next()
           }, config.tinyDnsPreloadDataDir())
      })
      .post(function (req, res, next) {

         try{
            var record = require("records/TapiRecordSOAContact")(req.body.json)
            var dom = TapiDomain(DEFAULT_DOMAIN, config, config.tinyDnsPreloadDataDir())

            regen.waitModifierLogic(next, config, function(){

                 dom.records = [record]

                 dom.Flush(req.query.ts, function(err){
                    if(err)
                       return next(err)

                    app.InsertEvent(req, {e_event_type: "soa-contact-change", e_other: req.body.json.contact});

                    req.result = { "hash": TapiRecords.Hash(record) }
                    next()

                 })

            })

         }catch(ex) {
            next(ex)
         }

      })
      .delete(function (req, res, next) {
            req.params.domain = DEFAULT_DOMAIN
            deleteDomain(req, next, config.tinyDnsPreloadDataDir())
      })


    Array("domain","template").forEach(category=>{
         var dataDirectory = category == "domain" ? config.tinyDnsDataDir() : config.tinyDnsTemplateDir()

         var extraParams = {};
         if(category == "template")
            extraParams.templateMode = true;


          router.route( "/"+category+"s")
             .search(function(req, res, next) {
                var j = req.body.json
                try {
                  if(!j["domains"]) return next(new MError("NO_DOMAINS"))
                  if(!util.isArray(j["domains"])) return next(new MError("INVALID_DOMAINS"))

                  var re = Array()
                  var all = j["domains"].length
                  var processed = 0

                  j["domains"].forEach((q)=>{
                     var dom = TapiDomain(q, config, dataDirectory)
                     dom.Exists((result)=>{
                        processed++
                        if(result){
                            re.push(q)
                        }

                        if(processed >= all) {
                           req.result = re
                           next()
                        }
                     })

                  })

                }catch(ex){
                   next(ex)
                }

              })

            .get(function (req, res, next) {
                fs.readdir(dataDirectory, function(err, files){
                   if(err) return next(new MError("DATA_DIR_READ_ERROR", null, err))

                   var re = config.filterArrayForZoneFiles( files )

                   req.result = {"domains": re}
                   next()

                });
             }
            )
            .put(function (req, res, next) {
                var j = req.body.json
                try {
                  var dom = TapiDomain(j["domain"], config, dataDirectory)
                  dom.Create(function(err){
                    if(err) return next(new MError("CANT_CREATE_DOMAIN", null, err))

                    app.InsertEvent(req, {e_event_type: "dns-zone-insert", e_other: j.domain});

                    req.result = "ok"
                    next()
                  })

                }catch(ex){
                   next(ex)
                }

             }
          )

          function getDomainExpress(callback) {
             return function(req,res,next){
                try {
                 req.domain = TapiDomain(req.params.domain, config, dataDirectory)
                 callback(req,res,next)

                }catch(ex){
                   next(ex)
                }

             }

          }

          function lockUnlock(key) {
             return getDomainExpress(function(req,res,next){
                return req.domain[key](function(err){
                    if(err) return next(err)

                    app.InsertEvent(req, {e_event_type: "dns-zone-"+key, e_other: req.domain.domain});

                    req.result = "ok"
                    next()
                })

             })
          }

          router.post( "/"+category+"/:domain/lock", lockUnlock("Lock"))
          router.post( "/"+category+"/:domain/unlock", lockUnlock("Unlock"))


          router.get( '/'+category+'/:domain/tinydnsdata', function(req,res,next){

                 readDomain(req.params.domain, next, (dom)=>{

                    req.result = {"data": dom.AsTinyDnsData() }
                    next()
                 }, dataDirectory)

          })


          router.route( '/'+category+'/:domain')
             .search(getDomainExpress(function(req, res, next) {

                 req.domain.Exists((result)=>{
                    req.result = result
                    next()
                 })

              }))
             .post(function(req, res, next) {

                 // this one creates a new domain if it does not exists yet
                 // and initializes it with the supplied default typical data
                 // such as nameservers, cname and mx records

                 var json = req.body.json

                 var aExtraParams = simpleCloneObject(extraParams);
                 aExtraParams.clientIp = req.clientIp;

                 if((json["reset_host_type"])&&(json["reset"])) {
                    addRecordsToDomain(req, next, json["reset"], false, dataDirectory, json["reset_host_type"], aExtraParams)
                 } else if(json["reset"]) {
                    addRecordsToDomain(req, next, json["reset"], true, dataDirectory, false, aExtraParams)

                 } else if(json["template"]) {
                     var e = extend({}, aExtraParams);
                     e.templateMode = true;
                     readDomain(json.template, next, (dom)=>{
                        e.templateMode = false;

                        addRecordsToDomain(req, next, dom.AsJSON(false), true, dataDirectory, false, e);

                     }, config.tinyDnsTemplateDir(), e)


                 }
                 else
                    return next(MError("RESET_ZONE_INCORRECT_PARAMS"))

              })

             .put(function(req, res, next) {
                 dontResolve(req.body.json);
                 addRecordsToDomain(req, next, req.body.json, false, dataDirectory, false, extraParams)

              })

             .get(function(req, res, next) {

                 readDomain(req.params.domain, next, (dom)=>{

                    req.result = {"records": dom.AsJSON(true) }
                    next()
                 }, dataDirectory, extraParams)

             })

             .delete(function(req, res, next) {

                  deleteDomain(req, next, dataDirectory)

            })


          router.post( '/'+category+'/:domain/:record', function(req, res, next) {
             if(req.result) return next();

             var aExtraParams = simpleCloneObject(extraParams);
             aExtraParams.clientIp = req.clientIp;

             var recordCandidate = TapiRecords.FromJSON(req.body.json, config, undefined, req.params.domain, aExtraParams);
             if(!recordCandidate) return next(new MError("INVALID_RECORD"));

             var j = req.body.json
             if(!j["limit"])
                j["limit"] = 0

             readDomain(req.params.domain, next, (dom)=>{

                  var zonesToRebuild;

                  var found = 0
                  var nRecords = []
                  for(q of dom.records) {

                      var relay = false;

                      if(req.params.record == TapiRecords.Hash(q)) {
                        found++;
                        if(q.type == "CLONE")
                          return next(new MError("CLONE_RECORD_CANNOT_BE_CHANGED"));
                        if(app.parentToChildrenMap[req.params.domain]) {
                           var key = getParentChildKeyByRecord(q);
                           if(app.parentToChildrenMap[req.params.domain][key]) {
                              // this is a clone parent. type and host cannot be changed.
                              if(q.type != recordCandidate.type)
                                return next(new MError("CLONE_PARENT_TYPE_CANNOT_BE_CHANGED"));
                              if(q.host != recordCandidate.host)
                                return next(new MError("CLONE_PARENT_HOST_CANNOT_BE_CHANGED"));

                              zonesToRebuild = app.parentToChildrenMap[req.params.domain][key];
                           }
                        }

                        if((j.limit)&&(found >= j.limit))
                          relay = true;
                        else
                          nRecords.push(recordCandidate);
                      }
                      else
                        relay = true;

                      if(relay)
                        nRecords.push(q)

                  }

                  if(!found)
                    return next(new MError("RECORD_NOT_FOUND"));


                  regen.waitModifierLogic(next, config, function(){
                      dom.records = nRecords
                      dom.Flush(req.query.ts, function(err){
                        if(err) return next(err)

                        app.InsertEvent(req, {e_event_type: "dns-record-replaced", e_other: recordsToJson(recordCandidate)});

                        req.result = {"changed": found}
                        next()

                        if(zonesToRebuild) {
                           console.log("We've got some zones to rebuild.", zonesToRebuild);
                           doRebuildZones(req.query.ts, dom, dataDirectory, zonesToRebuild);
                        }
                      })
                  })


             }, dataDirectory)

          })

          router.delete( '/'+category+'/:domain/:record', function(req, res, next) {

                 var j = req.body.json
                 if(!j["limit"])
                    j["limit"] = 0

                 readDomain(req.params.domain, next, (dom)=>{

                      var stuffsToRemoveFromMap = [];

                      var key;
                      var found = 0
                      var nRecords = []
                      for(q of dom.records) {

                          if(((j.limit)&&(found >= j.limit)) || (req.params.record != TapiRecords.Hash(q))) {
                             nRecords.push(q)
                          }
                          else {
                             found++
                             if(app.parentToChildrenMap[req.params.domain]) {
                               var key = getParentChildKeyByRecord(q);
                               if(app.parentToChildrenMap[req.params.domain][key])
                                 return next(new MError("RECORD_HAS_CLONE_CHILDS"));
                             }

                             if(q.type == "CLONE") {
                                // removal of a clone record. it should be removed from the map
                                var key = getParentChildKeyByRecord(q);
                                stuffsToRemoveFromMap.push({parentZoneName:q.parentZone, key: key, childZoneName:req.params.domain});
                             }
                          }

                      }

                      if(!found)
                        return next(new MError("RECORD_NOT_FOUND"));


                      regen.waitModifierLogic(next, config, function(){
                          dom.records = nRecords
                          dom.Flush(req.query.ts, function(err){
                            if(err) return next(err)

                            app.InsertEvent(req, {e_event_type: "dns-record-removed", e_other: recordsToJson(nRecords)});

                            req.result = {"deleted": found}
                            next()


                            if(stuffsToRemoveFromMap.length > 0) {
                               stuffsToRemoveFromMap.forEach(x=>{
                                  removeStuffFromMap(x.parentZoneName, x.key, x.childZoneName);
                               })
                               saveParentChildrenMap();
                            }
                          })
                      })


                 }, dataDirectory)



          })

          function dontResolve(json){
             if(category == "template")
                json.dontResolve = true;
          }


    })


    router.post( "/build", function (req, res, next) {
         regen.regenerateTinydnsDataLogic(function(){
             regen.regenerateTinydnsDataStub(config, function(code){

                 if(code)
                    return next(new MError("REGEN_FAILED"))

                 app.InsertEvent(req, {e_event_type: "dns-zone-regenerate"});
                 req.sendOk();
             })

         })
    })

    var tgzHandler = require("tapi-tgz")(router, app)


    app.Cleanup = function() {

        return tgzHandler.CleanupDirs(["preload", "data", "template"])

    }

    app.Prepare = function(){
       readParentChildrenMap();
       return Promise.resolve();
    }


    return app



    function readDomain(domainName, next, callback, directory, extraParams) {

     try {
        var dom = TapiDomain(domainName, config, directory, extraParams)
        dom.Parse(function(err, records){
          if(err) {
            if(err instanceof MError)
              return next(err)

            return next(new MError("CANT_READ_DOMAIN", null, err))
          }

          callback(dom)

        })

      }catch(ex){
         next(ex)
      }
    }

    function deleteDomain(req, next, directory){
       if(app.parentToChildrenMap[req.params.domain])
          return next(new MError("ZONE_HAS_CLONE_CHILDS"));

       try {
        var dom = TapiDomain(req.params.domain, config, directory)
        regen.waitModifierLogic(next, config, function(){
            dom.Delete(function(err){
              if(err) return next(new MError("CANT_DELETE_ZONE", null, err))

              app.InsertEvent(req, {e_event_type: "dns-zone-removed", e_other: req.params.domain.domain});

              removeChildFromMaps(req.params.domain);

              req.result = "ok"
              next()
            })
        })

      }catch(ex){
         next(ex)
      }
    }

  function getLockFileName(name) {
     var re = name.replace(/[^a-z0-9]/, "")
     if(!re) re = "unknown"
     return "tapi-lockfile-" +re
  }

       function addRecordsToDomain(req, next, json, reset, dataDirectory, deleteRecordsFirst, extraParams) {

           var record = TapiRecords.FromJSONArray(json, config, undefined, req.params.domain, extraParams)
           if(!record) return

           if(app.config.get("clone_record_support")) {
             // note: we requery the datadir here, so the clone record lookups will refer to the real target
             return ensureCloneRecordsExist(req.params.domain, record, config.tinyDnsDataDir(), realWork);
           } else {
             for(var r of record) {
                if(r.type == "CLONE") {
                   return next(new MError("CLONE_RECORDS_ARE_NOT_SUPPORTED"));
                }
             }
             return realWork(null, {});
           }



           function realWork(err, attachToChildren){

                 if(err) return next(err);

                 regen.waitModifierLogic(realWorkHasFinished, config, insideModifier);

                 function realWorkHasFinished(err) {
                       if(err) return next(err);

                       if(attachToChildren) {
                           Object.keys(attachToChildren).forEach(parentZoneName=>{
                               if(!app.parentToChildrenMap[parentZoneName]) app.parentToChildrenMap[parentZoneName] = {};

                               Object.keys(attachToChildren[parentZoneName]).forEach(hostAndTypeKey=>{
                                  if(!app.parentToChildrenMap[parentZoneName][hostAndTypeKey])
                                     app.parentToChildrenMap[parentZoneName][hostAndTypeKey] = [];

                                  attachToChildren[parentZoneName][hostAndTypeKey].forEach(childZoneName=>{
                                      if(app.parentToChildrenMap[parentZoneName][hostAndTypeKey].indexOf(childZoneName) < 0)
                                         app.parentToChildrenMap[parentZoneName][hostAndTypeKey].push(childZoneName);
                                  });

                               })
                           });

                           saveParentChildrenMap();

                       }

                       return next();
                 }



           function insideModifier(){


                 try {

                    var tmpPath = path.join(require('os').tmpdir(), getLockFileName(req.params.domain))
                    var lockFile = require('lockfile')

                    lockFile.lock(tmpPath,{wait: 5000}, function(err){

                      if(err) return next(new MError("INTERNAL_ERROR", null, err))

                      var dom = TapiDomain(req.params.domain, config, dataDirectory, extraParams);

                      if(reset) {
                         dom.records = record
                         FlushDomain()
                      }
                      else
                        dom.Parse(function(err, records){

                           if(err) {
                              lockFile.unlock(tmpPath, ()=>{})

                              return next(new MError("CANT_READ_DOMAIN", null, err))
                           }

                           if((deleteRecordsFirst)&&(deleteRecordsFirst.type)) {
                             deleteRecordsFirst.host = deleteRecordsFirst.host || ""

                             var newset = []
                             dom.records.forEach(rec=>{
                                if((rec.host == deleteRecordsFirst.host)&&(rec.type == deleteRecordsFirst.type))
                                   return
                                newset.push(rec)
                             })
                             dom.records = newset
                           }

                           dom.records = dom.records.concat(record)
                           FlushDomain()
                      })



                      function FlushDomain(){
                           dom.Flush(req.query.ts, function(err){

                              lockFile.unlock(tmpPath, ()=>{

                                if(err)
                                   return next(err)

                                var re = Array()
                                for(r of record) {
                                   re.push(TapiRecords.Hash(r))
                                }

                                app.InsertEvent(req, {e_event_type: "dns-record-insert", e_other: recordsToJson(record)});

                                req.result = { "hash": re }
                                realWorkHasFinished()
                              })

                           })

                      }

                    })


                  }catch(ex){
                     next(ex)
                  }

              }

           }



       }

       function readDomainAsync(domainName, dataDirectory){
            var dom = TapiDomain(domainName, config, dataDirectory);

            return new Promise(function(resolve,reject){
                return dom.Parse(function(err, records){
                    if(err) return reject(err);

                    resolve(dom);
                })

            })
       }

       function ensureCloneRecordExists(domainCandidate, recordCandidate, dataDirectory, resolvedDomainZone){

          if(recordCandidate.type != "CLONE") return Promise.resolve();

          return Promise.resolve()
            .then(()=>{

                if(resolvedDomainZone) return resolvedDomainZone;

                return readDomainAsync(recordCandidate.parentZone, dataDirectory)
                  .catch(ex=>{
                      if(ex.code == "ENOENT")
                        throw new MError("PARENT_NOT_FOUND");
                      throw ex;
                  });
            })
            .then(resolvedDomainZone=>{
                  var gotIt;
                  resolvedDomainZone.records.some(rec=>{
                      if((rec.host != recordCandidate.parentHost)||(rec.type != recordCandidate.parentType))
                         return;

                      gotIt = rec;
                      rec.SetHost(recordCandidate.host);
                      recordCandidate.raw = rec.AsTinyDnsData(domainCandidate);
                      return true;
                  });

                  if(!gotIt)
                    throw new MError("PARENT_NOT_FOUND");

                  return true;
            })
       }

       function ensureCloneRecordsExist(domainCandidate, records, dataDirectory, next){
           var re = {};
           var ps = [];
           records.forEach(record=>{
              ps.push(
                ensureCloneRecordExists(domainCandidate, record, dataDirectory)
                .then((shouldBeSaved)=>{
                    if(!shouldBeSaved) return;
                    if(!re[record.parentZone]) re[record.parentZone] = {};
                    var key = getParentChildKeyByRecord(record);
                    if(!re[record.parentZone][key]) re[record.parentZone][key] = [];
                    if(re[record.parentZone][key].indexOf(domainCandidate) < 0)
                      re[record.parentZone][key].push(domainCandidate);
                })
              );
           })

           return Promise.all(ps)
             .then(()=>{
                next(null, re);
             })
             .catch(next)

       }


       function recordsToJson(nRecords){
           if(nRecords.AsJSON) return nRecords.AsJSON();
           return nRecords.map(x=>x.AsJSON());
       }


       function getParentChildKeyByRecord(record) {
          if(record.type == "CLONE")
             return record.parentHost + "-" + record.parentType;
          return record.host + "-" + record.type;
       }


       function saveParentChildrenMap() {
           var lockfileLib = require("MonsterLockfile");
           return lockfileLib.logic({lockfile:"__parent_child.z", callback: function(){
               return fs.writeFileAsync(config.getParentChildMapFilePath(), "#"+JSON.stringify(app.parentToChildrenMap));
           }})
       }
       function readParentChildrenMap() {

           app.parentToChildrenMap = {};
           require("FirstLineAsJSON")(config.getParentChildMapFilePath(), function(err, result){
               if(err) {
                   if(err.code == "ENOENT") return;
                   console.error("Unable to read parent to children map", err, "tying to recover.")
                   rebuildParentChildrenMap();
                   return;
               }

               app.parentToChildrenMap = result;
            })

       }

       function removeStuffFromMap(parentZoneName, key, childZoneName) {
            var i = app.parentToChildrenMap[parentZoneName][key].indexOf(childZoneName);
            if(i < 0) return false;

            app.parentToChildrenMap[parentZoneName][key].splice(i, 1);

            if(app.parentToChildrenMap[parentZoneName][key].length <= 0) {
              delete app.parentToChildrenMap[parentZoneName][key];
              if(Object.keys(app.parentToChildrenMap[parentZoneName]).length <= 0)
                delete app.parentToChildrenMap[parentZoneName];
            }

       }

       function removeChildFromMaps(childZoneName) {
          var found = 0;
          Object.keys(app.parentToChildrenMap).forEach(parentZoneName=>{
             Object.keys(app.parentToChildrenMap[parentZoneName]).forEach(key=>{
                while(true){
                    if(!removeStuffFromMap(parentZoneName, key, childZoneName))
                       break;
                    found++;
                }

             });
          });

          if(found > 0)
            saveParentChildrenMap();
       }

       function doRebuildZones(timestamp, parentDomain, dataDirectory, zonesToRebuild) {

           return dotq.linearMap({array: zonesToRebuild, catch:true, action: function(domainNameToRebuild){
               return readDomainAsync(domainNameToRebuild, dataDirectory)
                 .then(domainToRebuild=>{
                    var changes = 0;
                    var ps = [];
                    domainToRebuild.records.forEach(record=>{
                        if(record.type != "CLONE") return;
                        ps.push( ensureCloneRecordExists(domainNameToRebuild, record, dataDirectory, parentDomain)
                          .then((changed)=>{
                              if(changed) changes++;
                          })
                          .catch(ex=>{
                             console.error("Unable to change child domain record", domainNameToRebuild, record, ex);
                          })
                        );
                    })

                    return Promise.all(ps)
                     .then(()=>{
                        return domainToRebuild.Flush(timestamp, function(err){
                           if(err)
                              console.error("Unable to save changed child domain", domainNameToRebuild, err);
                        });
                     })
                 })
           }})

       }

}
