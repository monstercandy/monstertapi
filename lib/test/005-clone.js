require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../tapi-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"


  describe("clone record support", function(){

        var cloneParentHash;
        var cloneChildHash;

        it('by default, support for clone records should be turned off', function(done) {

                 mapi.post( "/domain/clonechild.hu", {reset:{type:"CLONE","parentZone": "cloneparentnonexistent.hu", "parentHost": ""}}, function(err, res, body){
                       assert.propertyVal(err, "message", "CLONE_RECORDS_ARE_NOT_SUPPORTED");
                       app.config.set("clone_record_support", true);
                       done();

                   })

        })


        it('creating the parent', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/cloneparent.hu", {reset:{type:"A","ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        cloneParentHash = res.result.hash[0];
                   })

             )
        })

        it('when the parent does not exist, we shall receive an exception', function(done) {

                 mapi.post( "/domain/clonechild.hu", {reset:{type:"CLONE","parentZone": "cloneparentnonexistent.hu", "parentHost": ""}}, function(err, res, body){
                       assert.propertyVal(err, "message", "PARENT_NOT_FOUND");
                       done();

                   })

        })


        it('creating the child', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/clonechild.hu", {reset:{type:"CLONE","parentZone": "cloneparent.hu", "parentHost": ""}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })


        it("learning the hash", function(){
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/clonechild.hu")
                   .then((res)=>{
                        cloneChildHash = res.result.records[0].hash;
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('querying the child, should show up as the same record type', function(done) {

                 mapi.getAsync( "/domain/clonechild.hu/tinydnsdata")
                   .then((res)=>{
                      console.log(res.result.data);
                      assert.equal(res.result.data,'+clonechild.hu:123.123.123.123:3600\n')
                      done();
                   })
                   .catch(done);

        })

        it('attempting to change the child', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/clonechild.hu/"+cloneChildHash, {type:"CNAME","cname": "www.index.hu"})
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "CLONE_RECORD_CANNOT_BE_CHANGED")
                 })
             )
        })



        it('the special zone file should not be listed', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domains" )
                   .then((res)=>{
                      assert.equal(res.result.domains.indexOf("__parent_child", ), -1)
                   })

             )
        })

        it('removing the parent record should not be possible anymore', function(done) {
                 mapi.deleteAsync( "/domain/cloneparent.hu/"+cloneParentHash, {} )
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "RECORD_HAS_CLONE_CHILDS")
                    done()
                 })
                 .catch(done)

        })

        it('removing the parent zone should not be possible anymore', function(done) {
                 mapi.deleteAsync( "/domain/cloneparent.hu", {})
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "ZONE_HAS_CLONE_CHILDS")
                    done();
                 })
                 .catch(done)

        })


        it('changing the parent\'s type should be rejected', function(done) {
                 mapi.postAsync( "/domain/cloneparent.hu/"+cloneParentHash, {type:"CNAME",host:"","cname":"www.index.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "CLONE_PARENT_TYPE_CANNOT_BE_CHANGED")
                    done();
                 })
                 .catch(done)
        })

        it('changing the parent\'s host should be rejected', function(done) {
                 mapi.postAsync( "/domain/cloneparent.hu/"+cloneParentHash, {type:"A",host:"www","ip":"8.8.8.8"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "CLONE_PARENT_HOST_CANNOT_BE_CHANGED")
                    done();
                 })
                 .catch(done)

        })

        it('changing the parent should change the childs', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/domain/cloneparent.hu/"+cloneParentHash, {type:"A",host:"","ip":"8.8.8.8"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('querying the child, change should be reflected', function(done) {

          // lets give the rebuild process some time
          setTimeout(function(){


                 mapi.getAsync( "/domain/clonechild.hu/tinydnsdata")
                   .then((res)=>{
                      console.log(res.result.data);
                      assert.equal(res.result.data,'+clonechild.hu:8.8.8.8:3600\n')
                      done();
                   })
                   .catch(done);

          }, 500);

        })

        it('removing the child entry should be rejected with the old hash', function(done) {
                 mapi.deleteAsync( "/domain/clonechild.hu/"+cloneChildHash, {})
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "RECORD_NOT_FOUND")
                    done();
                 })
                 .catch(done)

        })

        it("learning the hash", function(){
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/clonechild.hu")
                   .then((res)=>{
                        cloneChildHash = res.result.records[0].hash;
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('removing the child entry', function() {
             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/clonechild.hu/"+cloneChildHash, {})
             )

        })



        it('removing the parent should now be possible', function(done) {
            setTimeout(function(){
               mapi.deleteAsync( "/domain/cloneparent.hu", {} )
               .then(()=>{
                  done();
               })
               .catch(done)
            }, 500);

        })
});


describe("removal by zone", function(){


        it('creating the parent again', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/cloneparent.hu", {reset:{type:"A","ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('creating the child again', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/clonechild.hu", {reset:{type:"CLONE","parentZone": "cloneparent.hu", "parentHost": ""}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('removing the child zone', function() {
             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/clonechild.hu/", {})
             )

        })

        it('removing the parent should now be possible again', function(done) {
            setTimeout(function(){
                 mapi.deleteAsync( "/domain/cloneparent.hu", {} )
                 .then(()=>{
                    done();
                 })
                 .catch(done)
            }, 500);

        })
})

describe("clone record support (not root-record)", function(){


        it('same test with a non-root record, parent', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/domain/clonesubparent.hu", {reset:{host:"parent",type:"A","ip":"9.9.9.9"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('creating the sub child with invalid host spec should fail', function(done) {

                 mapi.postAsync( "/domain/clonesubchild.hu", {reset:{host:"child",type:"CLONE","parentZone": "clonesubparent.hu", "parentHost": ""}})
                 .then(()=>{
                    throw new Error("Should not");
                 })
                 .catch(ex=>{
                    assert.propertyVal(ex, "message", "PARENT_NOT_FOUND")
                    done();
                 })
                 .catch(done)
        })
        it('creating the sub child', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/clonesubchild.hu", {reset:{host:"child",type:"CLONE","parentZone": "clonesubparent.hu", "parentHost": "parent"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('querying the child should look like as expected', function(done) {
              mapi.getAsync( "/domain/clonesubchild.hu/tinydnsdata")
                   .then((res)=>{
                      console.log(res.result.data);
                      assert.equal(res.result.data,'+child.clonesubchild.hu:9.9.9.9:3600\n')
                      done();
                   })
                   .catch(done);
        })
  })

  describe("clone records and zone templates", function(){

        it('creating the parent', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/service-primary-domain.hu", {reset:{type:"A","host":"s1","ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('new template with zone', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/template/clone-template.hu", {reset:[
                    {"type":"CLONE",host:"","parentZone": "service-primary-domain.hu", "parentHost": "s1"},
                    {"type":"CNAME", "host":"www","cname":"{{zone}}"},
                    ]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('and a customer domain', function() {


             return assert.isFulfilled(
                 mapi.postAsync( "/domain/customer.hu", {template:"clone-template.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('and now lets see what was generated', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/customer.hu/tinydnsdata")
                   .then((res)=>{
                      // console.log(res.result.data);
                      assert.equal(res.result.data,'+customer.hu:123.123.123.123:3600\nCwww.customer.hu:customer.hu:3600\n')
                   })

             )
        })

  })


})

