require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../tapi-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    
    "use strict"

    let domainsToTest = 1000
    app.config.set("max_request_body_size_import_export", 1000000)

    describe('preparing zones', function() {


        it('add zones', function(done) {

                 function doDomain(i){
                     "use strict"
                     if(i>= domainsToTest) return done()

                     let domain = "domain"+i+".tld"

                     mapi.put( "/domains", {"domain": domain}, function(err,res){
                         if(err) return done(err)

                         mapi.put( "/domain/"+domain, [{type:"A","ip":"123.123.123.123"},{"type":"CNAME","host":"www","cname":"index.hu"}], function(err){
                            if(err) return done(err)

                            return doDomain(i+1)
                         })
                     })
                 }


                 doDomain(0)

        })

    })




    describe('mass import/export', function() {

        var data
        it('exporting', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/export/tgz" )
                   .then((res)=>{
                      assert.property(res.result,"preload")
                      assert.property(res.result,"data")
                      data = res.result
                   })

             )
        })


        it('importing', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/import/tgz", data )
                   .then((res)=>{
                    console.log(res.result)
                       assert.equal(res.result.extracted, domainsToTest)
                   })

             )
        })

    })




}, 60000)

