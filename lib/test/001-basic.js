require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../tapi-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"


	describe('hostmaster', function() {

        setHostmaster()

        deleteHostmaster()

        setHostmaster()

	})

    describe('zones', function() {
        "use strict"

        for (let domain of ["testxxx", "áéláésd", "dsfo..gu"]) {

            it('add zone with invalid domain name '+q, function() {

                 return assert.isRejected(
                     mapi.putAsync( "/domains", {"domain": domain})
                 )
            })


        }


        addZone()

        it('deleting a not existing zone', function() {

             return assert.isRejected(
                 mapi.deleteAsync( "/domain/testsdf.hu", {})
             )
        })


        delZone()



        addZone()

    })


    describe('records', function() {

        it('posting without reset', function() {

             return assert.isRejected(
                 mapi.postAsync( "/domain/test.hu", {})
             )
        })


        it('posting with reset (single record syntax)', function() {

             return assert.isRejected(
                 mapi.postAsync( "/domain/test.hu", {reset:{}})
             )
        })

        it('posting with reset (array syntax)', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test.hu", {reset:[]})
             )
        })

        it('posting A record without IP', function() {

             return assert.isRejected(
                 mapi.postAsync( "/domain/test.hu", {reset:{type:"A"}})
             )
        })


        for(let ip of ["123", "123.", "ssdf", "123.sdf", "127.0.0.1.123"]) {
            it('post with incorrect IP '+ip, function() {

                 return assert.isRejected(
                     mapi.postAsync( "/domain/test.hu", {reset:{type:"A","ip":ip}})
                 )
            })
        }

        it('post with correct IP (single record syntax)', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test.hu", {reset:{type:"A","ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, ["087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646"])

                   })

             )
        })


        it('post with correct IP (array syntax)', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test.hu", {reset:[{type:"A","ip":"123.123.123.123"},{"type":"CNAME","host":"www","cname":"index.hu"}]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, [ '087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646',
     'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' ])
                   })

             )
        })

        const underScoreHash = 'fda724c9cb2aa46b8ede25caaa11877dbfde0721b7f4727b178e8d4f87b7fe8e';
        it('subdomain with underscore', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/domain/test.hu", {"type":"CNAME","host":"_sip","cname":"index.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, [ underScoreHash ])
                   })

             )
        })

        it('and removal', function() {

             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/test.hu/"+underScoreHash, {})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        var templateHashes = [ '1b3a40c805f28391d0575822e1e9b95b37d68c70f0d903c24894ed22782817a6',
     '9885d05c65d789fbabff00fff95eafee4428b8270c8037ce1502cf7b9ebc2355',
     '087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646',
     'b59b6c9aef7fba2169d524f48415f0cadedfb41a59c5402e6406138a73436f64' ]
        it('new template', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/template/nonamedomain.hu", {reset:[
                    {"type":"SOA/NS/A", "nameserver":"a.ns.nonamedomain.hu"},
                    {"type":"SOA/NS/A", "nameserver":"b.ns.nonamedomain.hu"},
                    {"type":"A", "ip":"123.123.123.123"},
                    {"type":"CNAME", "host":"www","cname":"nonamedomain.hu"},
                    ]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, templateHashes)
                   })

             )
        })


        it('reset domain to template defaults', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/something-else.hu", {template:"nonamedomain.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, templateHashes)
                   })

             )
        })

    })




    describe('import/export', function() {

        var data

        it('exporting as json', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/export/json" )
                   .then((res)=>{
                    console.log(res.result);
                      Array('something-else.hu', 'test.hu').forEach(function(d){
                         assert.ok(res.result[d]);
                         assert.ok(res.result[d].mtime);
                         delete res.result[d].mtime;
                      });
                      
                      assert.deepEqual(res.result, { 'something-else.hu': { records:
   [ { type: 'SOA/NS/A',
       ttl: 3600,
       nameserver: 'a.ns.nonamedomain.hu' },
     { type: 'SOA/NS/A',
       ttl: 3600,
       nameserver: 'b.ns.nonamedomain.hu' },
     { type: 'A', ttl: 3600, ip: '123.123.123.123' },
     { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'nonamedomain.hu' } ] },
  'test.hu': { records: 
   [ { type: 'A', ttl: 3600, ip: '123.123.123.123' },
     { type: 'CNAME', ttl: 3600, host: 'www', cname: 'index.hu' } ] } });
                   })

             )
        })

        it('exporting as tgz', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/export/tgz" )
                   .then((res)=>{
                      assert.property(res.result,"preload")
                      assert.property(res.result,"data")
                      assert.property(res.result,"template")
                      data = res.result
                   })

             )
        })

        deleteHostmaster()
        delZone()

        it('deleting template', function() {

             return assert.isFulfilled(
                 mapi.deleteAsync( "/template/nonamedomain.hu", {})
             )
        })

        it('importing', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/import/tgz", data )
                   .then((res)=>{
                       assert.equal(res.result.extracted, 4) // 4 means the templates were recovered as well
                   })

             )
        })

    })



    describe('query records', function() {

        it('query zone', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/test.hu" )
                   .then((res)=>{

                       assert.sameDeepMembers(res.result.records, [ { type: 'A',
       ttl: 3600,
       ip: '123.123.123.123',
       hash: '087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646' },
     { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'index.hu',
       hash: 'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' } ])
                   })

             )
        })


        it('query soa', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/soa-contact" )
                   .then((res)=>{
                       console.log(res.result)
                       assert.equal(res.result.contact, 'hostmaster.monster' )
                   })

             )
        })

    })



    describe('delete records', function() {


        it('trying to delete a non-existing record', function() {
             return assert.isRejected(
                 mapi.deleteAsync( "/domain/test.hu/087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860fffff", {} )
             )
        })


        it('trying to delete a record', function() {
             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/test.hu/087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646", {} )
             )
        })


        it('duplicating a record', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/domain/test.hu", {"type":"CNAME","host":"www","cname":"index.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, [ 'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' ])
                   })

             )
        })

        it('limiting the number of deleted records #1', function() {
             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/test.hu/cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451", {limit: 1} )
             )
        })

        it('limiting the number of deleted records #2', function() {
             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/test.hu/cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451", {limit: 1} )
             )
        })
        it('limiting the number of deleted records #3', function() {
             return assert.isRejected(
                 mapi.deleteAsync( "/domain/test.hu/cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451", {limit: 1} )
             )
        })

    })


    describe("extra tests", function(){

        it('cname with uppercased hostname', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test.hu", {reset:[{"type":"CNAME","host":"www","cname":"ORIGO.HU"}]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, ['5786fd99f20d8e171222fd4aa1e4d02931c1f9802f65cbcc311c00ffdd9a6f93' ])
                   })

             )
        })


        it('query the same zone, hostname should be lowercased', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/test.hu" )
                   .then((res)=>{

                       assert.sameDeepMembers(res.result.records, [ { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'origo.hu',
       hash: '5786fd99f20d8e171222fd4aa1e4d02931c1f9802f65cbcc311c00ffdd9a6f93' } ])
                   })

             )
        })



    })




    describe('max records per zone', function() {

        var baserecord = {type:"A","ip":"123.123.123.123"}

        it('should accept zones with '+app.config.get("max_number_of_records_per_zone")+" records", function() {

            var records = []
            for(var i = 0; i < app.config.get("max_number_of_records_per_zone"); i++) {
                records.push(baserecord)
            }

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/maxrecords.hu", {reset:records})
             )
        })

        it('but should reject with '+(app.config.get("max_number_of_records_per_zone")+1)+" records", function() {

            var records = []
            for(var i = 0; i < app.config.get("max_number_of_records_per_zone")+1; i++) {
                records.push(baserecord)
            }

             return assert.isRejected(
                 mapi.postAsync( "/domain/maxrecords.hu", {reset:records})
             )
        })


    })


    describe('domain locking', function() {

        var baserecord = {type:"A","ip":"123.123.123.123"}

        it('should accept record first', function(done) {

                 mapi.postAsync( "/domain/locking.hu", {reset:baserecord})
                   .then(()=>{
                      done()
                   })
                   .catch(done)
        })

        it('locking should return ok', function(done) {
                 mapi.postAsync( "/domain/locking.hu/lock", {})
                   .then(()=>{
                      done()
                   })
                   .catch(done)

        })


        it('changes then should be rejected', function(done) {

                 mapi.postAsync( "/domain/locking.hu", {reset:baserecord})
                   .then(()=>{
                      done(new Error("should have failed"))
                   })
                   .catch(err=>{
                      assert.propertyVal(err, "message", "DOMAIN_IS_LOCKED")
                      done()
                   })
        })

        it('unlocking should return ok', function(done) {
                 mapi.postAsync( "/domain/locking.hu/unlock", {})
                   .then(()=>{
                      done()
                   })
                   .catch(done)

        })


        it('changes should be accepted again', function(done) {

            console.log("doing this last")

                 mapi.postAsync( "/domain/locking.hu", {reset:[baserecord,baserecord]})
                   .then(()=>{
                        done()
                   })
                   .catch(done)
        })

    })



    function deleteHostmaster() {

        it('delete', function() {

             return assert.isFulfilled(
                 mapi.deleteAsync( "/soa-contact", {})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.equal(res.result, "ok")
                   })
             )
        })


    }

    function setHostmaster(){
        it('set', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/soa-contact", {contact: "hostmaster.monster"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.propertyVal(res.result, "hash", "0c8bb2f7884bf1810867b6ee18f7ff20c1849e03a759ed1d2f4f6f9ed712a7b8")

                   })
             )
        })

    }

    function addZone(){
        it('add zone', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/domains", {domain: "test.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.equal(res.result, "ok")

                   })
             )
        })

    }

    function delZone(){
        it('deleting existing zone', function() {

             return assert.isFulfilled(
                 mapi.deleteAsync( "/domain/test.hu", {})
             )
        })

    }

})

