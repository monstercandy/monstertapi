require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../tapi-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    "use strict"

    describe('SRV', function() {
        "use strict"

        it('add zone', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/domains", {domain: "test.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.equal(res.result, "ok")
                   })
             )
        })


        it('SRV record add', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test.hu", {reset:{type:"SRV","host":"_sip._tcp","priority":10,"weight":100,"port":5060,"target":"pbx.example.com","ttl":86400}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, ["f05e3368a36b93f7080ccbcca5022108363ec451af9e282a771eb39c2b52b0ec"])

                   })

             )
        })


        it('SRV record verify', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/test.hu/tinydnsdata")
                   .then((res)=>{
                      assert.equal(res.result.data,':_sip._tcp.test.hu:33:\\000\\012\\000d\\023\\304\\003pbx\\007example\\003com\\000:86400\n')
                   })

             )
        })


    })

    describe("host with wildcard", function(){

        it('adding host cname with wildcard *', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test.hu", {reset:{type:"CNAME","host":"*","cname":"anything-else.hu"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")

                   })

             )
        })


        it('reading back the domain should be fine', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/test.hu")
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })
    })


    describe('Long TXT', function() {
        "use strict"

        it('add zone', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/domains", {domain: "test2.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.equal(res.result, "ok")
                   })
             )
        })


        it('Long TXT record add', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/test2.hu", {reset:{"type":"TXT","ttl":"600","host":"mail._domainkey","text":"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxQ8oqucrtUbwNF8QENxVyrIJgdHLFeEWBIUYiKvGX3RFhCbT66eeM8/9JajjwxySb2qpAPAso1Y5FmhLPiytq+rmyQknr3BTyLRrppt3coTNYsNmsMy3HwCASBgPa77gS5w0gwog1qF+Mocmc61Elg0zlIlSbZ5OU05JOAdY5wH29LZUjAMGChhUBMFMF5cpuB5044EJUHnl8GiEdyoFfoUY2Vq3sEIuIjJRoeNmws0KrKjB3ZBZoXpg7AEDSRBAJWhTVeP/hqWiRVdjEs/wIgFtJa228CxS5e99imPZV0a4jtn/4NGQfcGxuiPuX45fNoypbEyIfaRscw/VpVsVwQIDAQAB"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                        assert.sameMembers(res.result.hash, ["c7f164d79ef84f9cc1780e3f0277d47fc51f30c1f42cbc7a7b8029edcfdca253"])

                   })

             )
        })


        it('Long TXT record verify', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/test2.hu/tinydnsdata")
                   .then((res)=>{
                      assert.equal(res.result.data,':mail._domainkey.test2.hu:16:\\377v=DKIM1;\\040k=rsa;\\040p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxQ8oqucrtUbwNF8QENxVyrIJgdHLFeEWBIUYiKvGX3RFhCbT66eeM8\\0579JajjwxySb2qpAPAso1Y5FmhLPiytq+rmyQknr3BTyLRrppt3coTNYsNmsMy3HwCASBgPa77gS5w0gwog1qF+Mocmc61Elg0zlIlSbZ5OU05JOAdY5wH29LZUjAMGChhUBMFMF5cpu\\233B5044EJUHnl8GiEdyoFfoUY2Vq3sEIuIjJRoeNmws0KrKjB3ZBZoXpg7AEDSRBAJWhTVeP\\057hqWiRVdjEs\\057wIgFtJa228CxS5e99imPZV0a4jtn\\0574NGQfcGxuiPuX45fNoypbEyIfaRscw\\057VpVsVwQIDAQAB:600\n')
                   })

             )
        })
    })



    describe('CAA records', function() {
        "use strict"

        it('add zone', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/domains", {domain: "caa.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.equal(res.result, "ok")
                   })
             )
        })


        it('add caa record', function(done) {

                 mapi.post( "/domain/caa.hu", {reset:
                  {"type":"CAA","ttl":"600","host":"","flags":1,"tag":"issue","value":"ca.example.net"}
                 }, function(err, result){
                    // console.log(err, result)
                        assert.property(result, "hash")
                        assert.sameMembers(result.hash, ["2a91fc74d9f3c239fc852f47e010ba3a0dcb9f51a1982c41e7f959b93b3923e4"])
                        done()
                 })
        })


        it('CAA record verify', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/caa.hu/tinydnsdata")
                   .then((res)=>{
                      assert.equal(res.result.data,':caa.hu:257:\\001\\005issueca\\056example\\056net:600\n')
                   })

             )
        })
    })





})

