require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../tapi-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

  describe("A record without ip address", function(){
        it('create A record without IP', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/withouta.hu", {reset:{useRequestIp: true, type:"A"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })    

        it('retrieving it to see if ip was populated', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/withouta.hu")
                   .then((res)=>{
                    console.log(res.result);
                        assert.deepEqual(res.result, { records:
   [ { type: 'A',
       ttl: 3600,
       ip: '127.0.0.1',
       hash: '47152ef6b43496c738bdfa35b4bcb9e400f30b1a384b6d8ffb5564edef6a33ef' } ] })
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

  })

  describe('modifying records', function() {

        it('create a record first', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/tobemodded.hu", {reset:{type:"A","ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('retrieving it before', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/tobemodded.hu")
                   .then((res)=>{
                        assert.deepEqual(res.result, { records:
   [ { type: 'A',
       ttl: 3600,
       ip: '123.123.123.123',
       hash: '087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646' } ] })
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('change it after to something else (invalid hash)', function(done) {
                 mapi.postAsync( "/domain/tobemodded.hu/foobar", {type:"CNAME",host:"www","cname":"index.hu"})
                   .catch(ex=>{
                      assert.propertyVal(ex, "message", "RECORD_NOT_FOUND");
                      done();
                   })
                   .catch(done)

        })

        it('change it after to something else', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/domain/tobemodded.hu/087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646", {type:"CNAME",host:"www","cname":"index.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('retrieving it after', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/tobemodded.hu")
                   .then((res)=>{
                    console.log(res.result);
                        assert.deepEqual(res.result, { records:
   [ { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'index.hu',
       hash: 'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' } ] })
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })
  })

  describe("strict mx records", function(){

        it('should be accepted when it is the same domain', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/mxtest.hu", {reset:{type:"MX","mailserver": "mxtest.hu", "ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('should be accepted when it is a subdomain', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/mxtest.hu", {reset:{type:"MX","mailserver": "subdomain.mxtest.hu", "ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('should be accepted when it is a pseudo subdomain (a.mx.mxtest.hu)', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/mxtest.hu", {reset:{type:"MX","mailserver": "a", "ip":"123.123.123.123"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })


        it('should be rejected when an IP address is specified for an external mailserver', function(done) {

             mapi.post( "/domain/mxtest.hu", {reset:{type:"MX","mailserver": "external.hu", "ip":"123.123.123.123"}}, function(err, body){
                assert.propertyVal(err, "message", "IP_NOT_ALLOWED_FOR_EXTERNAL_MAILSERVER")
                done()
             })

        })

        it('but it shouold be still ok without an IP address', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/mxtest.hu", {reset:{type:"MX","mailserver": "external.hu"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })
  })


	describe('reset_host_Type', function() {


        it('creating a full set first', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/host-type.hu", {reset:[
                    {type:"A","ip":"123.123.123.123"},
                    {type:"A",host:"www","ip":"123.123.123.124"},
                    {type:"A",host:"www","ip":"123.123.123.125"},
                    {type:"CNAME",host:"www","cname":"index.hu"}
                  ]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                   })

             )
        })


        it('query zone', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/host-type.hu" )
                   .then((res)=>{
                       // console.log(res.result)
                       assert.sameDeepMembers(res.result.records, [
     { type: 'A',
       ttl: 3600,
       ip: '123.123.123.123',
       hash: '087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646' },
     { type: 'A',
       ttl: 3600,
       host: 'www',
       ip: '123.123.123.124',
       hash: '76b248006d6824b52f5cf94c89685eed6708886992ba2900099bf78801ea4712' },
     { type: 'A',
       ttl: 3600,
       host: 'www',
       ip: '123.123.123.125',
       hash: '6f0a00d1b9ae77c2601be25141fa6e41df6b34bac5b7bdadfcb498450563a93b' },
     { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'index.hu',
       hash: 'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' } ])
                   })

             )
        })


        it('changing www A records', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/host-type.hu", {
                  reset_host_type: {host:"www",type:"A"},
                  reset:[
                    {type:"A",host:"www","ip":"1.1.1.1"},
                  ]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                   })

             )
        })


        it('query zone', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/host-type.hu" )
                   .then((res)=>{
                       // console.log(res.result)
                       assert.sameDeepMembers(res.result.records, [
     { type: 'A',
       ttl: 3600,
       ip: '123.123.123.123',
       hash: '087ffcdfb4932ad9e84c791e24f76b5fc9143064d3e5a755f812998e860f3646' },
     { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'index.hu',
       hash: 'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' },
     { type: 'A',
       ttl: 3600,
       host: 'www',
       ip: '1.1.1.1',
       hash: '9c9a6dd02cf363929b57220153a86ce075e8db2ec0576d244397cdb625d9708c' } ])
                   })

             )
        })

        it('changing root A records', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/host-type.hu", {
                  reset_host_type: {type:"A"},
                  reset:[
                    {type:"A",host:"www","ip":"2.2.2.2"},
                  ]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                        assert.property(res.result, "hash")
                   })

             )
        })

        it('query zone', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/domain/host-type.hu" )
                   .then((res)=>{
                       // console.log(res.result)
                       assert.sameDeepMembers(res.result.records, [
     { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'index.hu',
       hash: 'cb6c642256f574969620f54094fc14f8f5bc63609fc8bbbb29710c43d7d71451' },
     { type: 'A',
       ttl: 3600,
       host: 'www',
       ip: '1.1.1.1',
       hash: '9c9a6dd02cf363929b57220153a86ce075e8db2ec0576d244397cdb625d9708c' },
     { type: 'A',
       ttl: 3600,
       host: 'www',
       ip: '2.2.2.2',
       hash: '37089d538f821454f9cfc57663179c477641db78783a391dadc669fcb93ae5b7' } ])
                   })

             )
        })


    })


  describe('misc', function() {

        it('host with boolean value should be rejected', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/boolean-host.hu", {reset:[
                    {type:"A","ip":"123.123.123.123",host:true},
                  ]})
                   .then(()=>{
                      throw new MError("SHOULD_HAVE_FAILED")
                   })
                   .catch(ex=>{
                      assert.propertyVal(ex, "message", "RECORD_HOST_INVALID")
                   })

             )
        })

        it('but it should be fine as string', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/boolean-host.hu", {reset:[
                    {type:"A","ip":"123.123.123.123",host:"true"},
                  ]})

             )
        })

  })

  describe("cname {{zone}} feature", function(){

        it('creating a domain with a cname record and {{zone}} content', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/cname-zone.hu", {reset:{type:"CNAME",host:"www","cname":"{{zone}}"}})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('quering the zone just created', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/cname-zone.hu/")
                   .then((res)=>{
                        assert.equal(res.result.records[0].cname, "cname-zone.hu");
                   })

             )
        })

        it('new template with zone', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/template/template-zone.hu", {reset:[
                    {"type":"CNAME", "host":"www","cname":"{{zone}}"},
                    ]})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('quering the template just created (the expression should have not been resolved)', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/template/template-zone.hu/")
                   .then((res)=>{
                        assert.equal(res.result.records[0].cname, "{{zone}}");
                   })

             )
        })


        it('new record into the template', function() {

             return assert.isFulfilled(
                 mapi.putAsync( "/template/template-zone.hu", {"type":"CNAME","host":"www","cname":"index.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })


        it('quering the template once again (the expression should still be the same)', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/template/template-zone.hu/")
                   .then((res)=>{
                        assert.equal(res.result.records[0].cname, "{{zone}}");
                   })

             )
        })


        it('reset domain to template defaults', function() {

             return assert.isFulfilled(
                 mapi.postAsync( "/domain/cname-zone2.hu", {template:"template-zone.hu"})
                   .then((res)=>{
                        assert.equal(res.httpResponse.statusCode, 200)
                   })

             )
        })

        it('quering the zone just restored based on the template', function() {

             return assert.isFulfilled(
                 mapi.getAsync( "/domain/cname-zone2.hu/")
                   .then((res)=>{
                        // console.log(res.result);
                        assert.equal(res.result.records[0].cname, "cname-zone2.hu");
                   })
             )
        })


  })


})

