const MError = require("MonsterError")

function TapiRecord(json, config, expectedType, tinyDnsLetter) {
  var m = { "type": expectedType, "tinydns_letter": tinyDnsLetter }

  if(json["type"] != expectedType)
  	throw new MError("RECORD_TYPE_MISMATCH")

  if(!json["host"])
  	json["host"] = ""


  if(json["host"]){
     if(typeof json["host"] != "string")
        throw new MError("RECORD_HOST_INVALID")

     json["host"] = require('MonsterValidators').TurnIntoValidHostWildcard(json["host"])
     if(!json["host"])
  	    throw new MError("HOST_INVALID")
  }

  m["host"] = json["host"]

  if(!json["ttl"])
     json["ttl"] = config.get("dns_record_default_ttl")
  if((!String(json["ttl"]).match(/^\d+$/))||(json["ttl"] <= 0))
  	throw new MError("RECORD_INVALID_TTL")

  m["ttl"] = json["ttl"]

  m.AsJSON = function(){
    var r = {"type":m["type"],"ttl": m["ttl"]}
	if(m["host"])
	  r["host"] = m["host"]
  	return r
  }

  m.AsTinyDnsData = function(dom){
  	var fq = m.GetFQDN(dom, m["host"])
  	return `${m['tinydns_letter']}${fq}`
  }

  m.GetFQDN = function(dom, host) {
  	  return (host ? host + "." + dom : dom)
  }

  m.SetHost = function(host){
     m.host = host;
  }

  return m
}


module.exports = TapiRecord
