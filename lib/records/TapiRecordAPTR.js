const TapiRecordAPTRBase = require("records/TapiRecordAPTRBase.js")

function TapiRecordAPTR(json, config) {
  var m = { }

  m.__proto__ = TapiRecordAPTRBase(json, config, "A/PTR", "=") 

  return m
}

module.exports = TapiRecordAPTR
