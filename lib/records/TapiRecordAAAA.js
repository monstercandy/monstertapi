const TapiRecordGeneric = require("records/TapiRecordGeneric.js")
const MError = require("MonsterError")

function TapiRecordAAAA(json, config) {
  var m = { }

  if(!json["ip"])
  	throw new MError("RECORD_MISSING_IP")
  var bin = require('MonsterValidators').IsValidIPv6(json["ip"])
  if(bin === false) 
  	throw new MError("RECORD_INVALID_IP")
    
  m["ip"] = json["ip"]
  json["data"] = bin

  json["rtype"] = 28
  m.__proto__ = TapiRecordGeneric(json, config, "AAAA") 


  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
    delete b["data"]
    delete b["rtype"]
    b["ip"] = m["ip"]
  	return b;
  }

  return m
}

module.exports = TapiRecordAAAA

