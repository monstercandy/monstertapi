const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")

function TapiRecordAPTRBase(json, config, expectedType, tinyDnsLetter) {
  var m = { }

  m.__proto__ = TapiRecord(json, config,  expectedType, tinyDnsLetter)

  if(!json["ip"])
  	throw new MError("RECORD_MISSING_IP")
  if(!require('MonsterValidators').IsValidIPv4(json["ip"]))
  	throw new MError("RECORD_INVALID_IP")

  m["ip"] = json["ip"]

  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
  	b["ip"]=m["ip"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
  	var b = m.__proto__.AsTinyDnsData(dom)
  	return b + `:${m['ip']}:${m['ttl']}`
  }


  return m
}

module.exports = TapiRecordAPTRBase
