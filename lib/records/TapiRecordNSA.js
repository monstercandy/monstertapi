const TapiRecordNSBase = require("records/TapiRecordNSBase.js")

function TapiRecordNSA(json, config) {
  var m = { }

  m.__proto__ = TapiRecordNSBase(json, config,  "NS/A", "&") 

  return m
}

module.exports =TapiRecordNSA
