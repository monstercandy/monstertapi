const TapiRecordNSBase = require("records/TapiRecordNSBase.js")

function TapiRecordSOANSA(json, config) {
  var m = { }

  m.__proto__ = TapiRecordNSBase(json, config,  "SOA/NS/A", ".") 

  return m
}

module.exports =TapiRecordSOANSA
