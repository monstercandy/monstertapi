const Validators = require('MonsterValidators')
const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")

function TapiRecordMX(json, config, domainName) {
  var m = { }

  m.__proto__ = TapiRecord(json, config,  "MX", "@")

  if((json["ip"])&&(!Validators.IsValidIPv4(json["ip"])))
    throw new MError("RECORD_INVALID_IP")
  m["ip"] = json["ip"]

  if(!json["priority"]) json["priority"] = 0
  if(!String(json["priority"]).match(/^\d\d?$/)) throw new MError("RECORD_INVALID_PRIORITY")
  m["priority"] = json["priority"]

  if(!json["mailserver"])
    throw new MError("RECORD_MAIL_SERVER_MISSING")
  json["mailserver"] = Validators.TurnIntoValidHost(json["mailserver"])
  if(!json["mailserver"])
    throw new MError("RECORD_MAIL_SERVER_INVALID")
  m["mailserver"] = json["mailserver"]

  if((config.get("strict_mx_records")) && (json["ip"])) {
      // console.log("doing external mx record checks", m["mailserver"], domainName)

      var external = true
      if(
          (m["mailserver"].indexOf(".") < 0)  // simple string, like "a"
          ||
          (m["mailserver"] == domainName)
          ||
          (m["mailserver"].endsWith("."+domainName))
        ) {
           external = false
      }

      if(external)
         throw new MError("IP_NOT_ALLOWED_FOR_EXTERNAL_MAILSERVER")

  }

  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
  	b["ip"]=m["ip"]
    b["priority"]=m["priority"]
    b["mailserver"]=m["mailserver"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
  	var b = m.__proto__.AsTinyDnsData(dom)
  	return b + ":"+(m['ip']||"")+`:${m['mailserver']}:${m['priority']}:${m['ttl']}`
  }

  return m
}

module.exports =TapiRecordMX
