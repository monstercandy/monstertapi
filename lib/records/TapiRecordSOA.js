const Validators = require('MonsterValidators')
const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")

function TapiRecordSOA(json, config) {
  var m = { }

  m.__proto__ = TapiRecord(json, config,  "SOA", "Z") 

  if(!json["mname"])
    throw new MError("RECORD_MISSING_MNAME")
  json["mname"] = Validators.TurnIntoValidHost(json["mname"])
  if(!json["mname"])
    throw new MError("RECORD_INVALID_MNAME")
  m["mname"] = json["mname"]

  if(!json["rname"])
    throw new MError("RECORD_MISSING_RNAME")
  json["rname"] = Validators.TurnIntoValidHost(json["rname"]
  if(!json["rname"])
    throw new MError("RECORD_INVALID_RNAME")
  m["rname"] = json["rname"]
   
  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
  	b["mname"]=m["mname"]
    b["rname"]=m["rname"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
  	var b = m.__proto__.AsTinyDnsData(dom)
  	return b + `:${m['mname']}:${m['rname']}::::::${m['ttl']}`
  }

  return m
}

module.exports =TapiRecordSOA
