const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")
const TinyDns = require("TinyDns.js")

function TapiRecordClone(json, config) {
  var m = { }


  if(!json["parentZone"])
  	throw new MError("RECORD_MISSING_PARENT_ZONE")

  if(!json["parentHost"])
    json["parentHost"] = ""

  if(json["parentHost"]){
     if(typeof json["parentHost"] != "string")
        throw new MError("PARENT_RECORD_HOST_INVALID")

     json["parentHost"] = require('MonsterValidators').TurnIntoValidHostWildcard(json["parentHost"])
     if(!json["parentHost"])
        throw new MError("PARENT_HOST_INVALID")
  }

  if(!json["parentType"])
    json["parentType"] = "A";

  if(typeof json["parentType"] != "string")
     throw new MError("PARENT_TYPE_INVALID");

  if(!json["parentType"].match(/^(A|TXT)$/))
    throw new MError("PARENT_TYPE_INVALID");

  if((json["raw"])&&(typeof json["raw"] != "string"))
     throw new MError("PARENT_RAW_INVALID");

  m.__proto__ = TapiRecord(json, config,  "CLONE");

  m["parentZone"] = json["parentZone"];
  m["parentHost"] = json["parentHost"];
  m["parentType"] = json["parentType"];
  m["raw"] = json["raw"];

  m.AsJSON = function(){
    var b = m.__proto__.AsJSON();
    b["parentZone"] = m["parentZone"];
    b["parentHost"] = m["parentHost"];
    b["raw"] = m["raw"];
    return b;
  }

  m.AsTinyDnsData = function(dom){
    return m["raw"];
  }


  return m

}

module.exports = TapiRecordClone
