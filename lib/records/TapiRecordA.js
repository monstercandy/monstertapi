const TapiRecordAPTRBase = require("records/TapiRecordAPTRBase.js")

function TapiRecordA(json, config, mainDomain, extraParams) {
  var m = { }

  if((extraParams)&&(extraParams.clientIp)&&(!json.ip)&&(json.useRequestIp)) {
  	const ipv6loopback_prefix = '::ffff:';
  	var ip = extraParams.clientIp;
  	if(extraParams.clientIp.startsWith(ipv6loopback_prefix))
  		ip = ip.substr(ipv6loopback_prefix.length);
  	json.ip = ip;  	
  }


  m.__proto__ = TapiRecordAPTRBase(json, config, "A", "+") 

  return m
}

module.exports = TapiRecordA
