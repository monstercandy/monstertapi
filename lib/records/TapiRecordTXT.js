const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")
const TinyDns = require("TinyDns.js")

function TapiRecordTXT(json, config) {
  var m = { }


  if(!json["text"])
  	throw new MError("RECORD_MISSING_TXT")
  if(!require("MonsterValidators").IsString(json["text"]))
    throw new MError("RECORD_INVALID_TXT")
  json["text"] = json["text"].trim()
  if(!json["text"])
    throw new MError("RECORD_MISSING_TXT")
  
  if(json["text"].length > config.get("max_txt_record_length"))
    throw new MError("RECORD_TXT_TOO_LONG")
  m["text"] = json["text"]

  m.__proto__ = TapiRecord(json, config,  "TXT", m["text"].length > 120 ? ":" : "'") 
   
  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
  	b["text"]=m["text"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){


    var olen = m["text"].length

    var b
    if(olen > 120) {
       b = m.__proto__.AsTinyDnsData(dom)
       b += ":16:"
       b += TinyDns.ConvertLongStringToTinydns(m["text"])
       b += ":" + m["ttl"]
    } else {
       var text_encoded = TinyDns.ConvertBinaryToOctetStrings(m["text"])
       b = m.__proto__.AsTinyDnsData(dom)
       return b + `:${text_encoded}:${m['ttl']}`
    }

  	return b
  }

  return m
}

module.exports = TapiRecordTXT
