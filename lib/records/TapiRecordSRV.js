const TapiRecordGeneric = require("records/TapiRecordGeneric.js")
const MError = require("MonsterError")

function TapiRecordSRV(json, config) {
  var m = { }

  var bin = []
  for(var q of ["priority","weight","port"]) {
    if(!json[q])
      json[q] = 0
    if((!String(json[q]).match(/^\d+$/))||(json[q] < 0)||(json[q] > 65535))
      throw new MError("RECORD_INVALID_"+q.toUpperCase())
    m[q] = json[q]

    bin.push ( (json[q] & 0xFF00) >>> 8 )
    bin.push ( json[q] & 0xFF )
  }

  json["target"] = require('MonsterValidators').TurnIntoValidHost(json["target"])
  if(!json["target"])
    throw new MError("RECORD_INVALID_TARGET")
  m["target"] = json["target"]

  for(var q of m["target"].split(".")) {
     bin.push(q.length) 
     for(var i = 0; i < q.length; i++)
        bin.push(q.charCodeAt(i))
  }  
  bin.push(0)

  json["data"] = bin

  json["rtype"] = 33
  m.__proto__ = TapiRecordGeneric(json, config, "SRV") 


  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
    delete b["data"]
    delete b["rtype"]
    for(var q of ["priority","weight","port","target"]) {
       b[q] = m[q]
    }
  	return b;
  }

  return m
}

module.exports = TapiRecordSRV

