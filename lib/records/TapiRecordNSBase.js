const Validators = require('MonsterValidators')
const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")

function TapiRecordNSBase(json, config, expectedType, tinyDnsLetter) {
  var m = { }

  m.__proto__ = TapiRecord(json, config, expectedType, tinyDnsLetter) 

  if((json["ip"])&&(!Validators.IsValidIPv4(json["ip"])))
    throw new MError("RECORD_INVALID_IP")
  m["ip"] = json["ip"]

  if(!json["nameserver"])
    throw new MError("RECORD_NAMESERVER_MISSING")
  json["nameserver"] = Validators.TurnIntoValidHost(json["nameserver"])
  if(!json["nameserver"])
    throw new MError("RECORD_NAMESERVER_INVALID")
  m["nameserver"] = json["nameserver"]
   
  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
  	b["ip"]=m["ip"]
    b["nameserver"]=m["nameserver"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
  	var b = m.__proto__.AsTinyDnsData(dom)
  	return b + ":"+(m['ip']||"")+`:${m['nameserver']}:${m['ttl']}`
  }

  return m
}

module.exports =TapiRecordNSBase
