const TapiRecordGeneric = require("records/TapiRecordGeneric.js")
const MError = require("MonsterError")

function TapiRecordCAA(json, config) {
  var m = { }

  var bin = []

  if(json.flags === 0)
    bin.push(0)
  else
  if(json.flags === 1)
    bin.push(1)
  else
    throw new MError("invalid flags");

  if((!json.tag)||(["issue","issuewild","iodef"].indexOf(json.tag) < 0))
    throw new MError("invalid tag");

  bin.push(json.tag.length);

   for(var i = 0; i < json.tag.length; i++)
      bin.push(json.tag.charCodeAt(i))

  if(typeof json.value != "string")
    throw new MError("invalid value");

   for(var i = 0; i < json.value.length; i++)
      bin.push(json.value.charCodeAt(i))

  json["data"] = bin

  json["rtype"] = 257;
  Array("flags","tag","value").forEach(x=>{
     m[x] = json[x]
  })

  m.__proto__ = TapiRecordGeneric(json, config, "CAA")

  m.AsJSON = function(){
    var b = m.__proto__.AsJSON()
    delete b["data"]
    delete b["rtype"]
    Array("flags","tag","value").forEach(x=>{
       b[x] = m[x]
    })

    return b;
  }


  return m
}

module.exports = TapiRecordCAA

