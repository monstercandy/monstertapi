const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")

function TapiRecordGeneric(json, config, name) {
  var m = { }

  if(!name) name = "Generic"

  m.__proto__ = TapiRecord(json, config,  name, ":") 

/*must be between 1 and 65535; it must not be 2 (NS), 5 (CNAME), 6 (SOA), 12 (PTR), 15 (MX), or 252 (AXFR)*/
  if(!json["rtype"])
    throw new MError("RECORD_MISSING_RTYPE")
  if(!String(json["rtype"]).match(/^\d+$/))
    throw new MError("RECORD_RTYPE_INVALID_1")
  if((json["rtype"] < 1)|| (json["type"] > 65535))
    throw new MError("RECORD_RTYPE_INVALID_2")
  if(String(json["rtype"]).match(/^(2|5|6|12|15|252)$/))
    throw new MError("RECORD_RTYPE_INVALID_3")
  m["rtype"] = json["rtype"]


  if(!json["data"])
  	throw new MError("RECORD_MISSING_DATA")

  if(!json["data"].length > 255)
    throw new MError("RECORD_TOO_LONG")

  if(!Array.isArray(json["data"]))
    throw new MError("RECORD_INCORRECT_DATA")
  if(json["data"].length > config.get("max_data_record_length"))
    throw new MError("RECORD_DATA_TOO_LONG")
  for(q of json["data"]) {
    if(!String(q).match(/^\d+$/))
      throw new MError("RECORD_DATA_INVALID_DIGIT1")
    if((q < 0)||(q > 255))
      throw new MError("RECORD_DATA_INVALID_DIGIT2")
  }
  m["data"] = json["data"]

   
  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
    b["rtype"]=m["rtype"]
  	b["data"]=m["data"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
    var text_encoded = require("TinyDns.js").ConvertByteArrayToOctetStrings(m["data"])

    var b = m.__proto__.AsTinyDnsData(dom)
       b += ":"+m["rtype"]+":"
       b += text_encoded
       b += ":" + m["ttl"]

  	return b
  }

  return m
}

module.exports = TapiRecordGeneric
