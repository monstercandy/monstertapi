const TapiRecord = require("records/TapiRecord.js")
const MError = require("MonsterError")

function TapiRecordCNAME(json, config, mainDomain, extraParams) {
  var m = { }

  m.__proto__ = TapiRecord(json, config,  "CNAME", "C")

  if(!json["cname"])
    throw new MError("RECORD_MISSING_CNAME")

  if(json.cname == "{{zone}}") {
    if((!extraParams)||(!extraParams.templateMode))
      json.cname = mainDomain;
  }
  else
    json["cname"] = require('MonsterValidators').TurnIntoValidHost(json["cname"])

  if(!json["cname"])
    throw new MError("RECORD_INVALID_CNAME")

  m["cname"] = json["cname"]


  m.AsJSON = function(){
  	var b = m.__proto__.AsJSON()
  	b["cname"]=m["cname"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
  	var b = m.__proto__.AsTinyDnsData(dom)
  	return b + `:${m['cname']}:${m['ttl']}`
  }

  return m
}

module.exports =TapiRecordCNAME
