const TapiRecordAPTRBase = require("records/TapiRecordAPTRBase.js")

function TapiRecordPTR(json, config) {
  var m = { }

  m.__proto__ = TapiRecordAPTRBase(json, config,  "PTR", "^") 

  return m
}

module.exports = TapiRecordPTR
