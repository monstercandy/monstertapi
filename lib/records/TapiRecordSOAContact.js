const MError = require("MonsterError")

function TapiRecordSOAContact(json, config) {
  var m = { }
   

  if(!json["contact"])
    throw new MError("RECORD_MISSING_CONTACT")
  json["contact"] = require("MonsterValidators").TurnIntoValidHost(json["contact"])
  if(!json["contact"])
    throw new MError("RECORD_INVALID_CONTACT")
  m["contact"] = json["contact"]


  m.AsJSON = function() {
  	var b = {"type": "SOA-Contact"}
  	b["contact"]=m["contact"]
  	return b;
  }

  m.AsTinyDnsData = function(dom){
    return `D${m['contact']}`
  }

  return m
}

module.exports = TapiRecordSOAContact
